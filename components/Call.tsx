import React, {Component} from 'react';
import {
  Platform,
  ScrollView,
  Text,
  TouchableOpacity,
  View,
  BackHandler,
  Alert,
  Linking,
  Image,
  Button,
  AppState,
} from 'react-native';
import RtcEngine, {
  Color,
  RtcLocalView,
  RtcRemoteView,
  VideoRenderMode,
} from 'react-native-agora';

import requestCameraAndAudioPermission from './Permission';
import styles from './Style';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import IoniconsIcon from 'react-native-vector-icons/Ionicons';
import FeatherIcon from 'react-native-vector-icons/Feather';

import {useRoute} from '@react-navigation/native';
import dynamicLinks from '@react-native-firebase/dynamic-links';

interface Props {}

/**
 * @property peerIds Array for storing connected peers
 * @property appId
 * @property channelName Channel Name for the current session
 * @property joinSucceed State variable for storing success
 */
interface State {
  appId: string;
  channelName: string;
  joinSucceed: boolean;
  peerIds: number[];
  audioMuted: boolean;
  cameraSwitch: boolean;
  videoHide: boolean;
  isAppKill: boolean;
  isStreamOpen: boolean;
}

export default class Call extends Component<Props, State> {
  _engine?: RtcEngine;

  appId;

  constructor(props) {
    super(props);
    console.log(this.props);
    const {route} = this.props;
    console.log(route);
    (this.appId = '35f3d29a9cc445beb3c9fee509bf4ab6'),
      (this.state = {
        //  appId: '3535a5b6b0414c58ad4edab659badb72', //client working
        // channelName: '46648',

        appId: '35f3d29a9cc445beb3c9fee509bf4ab6',
        channelName: 'wtgtest',
        uid: '',
        joinSucceed: false,
        linkJoin: false,
        peerIds: [],
        audioMuted: false,
        videoHide: false,
        cameraSwitch: false,
        isAppKill: false,
        isStreamOpen: false,
      });

    if (Platform.OS === 'android') {
      // Request required permissions from Android
      requestCameraAndAudioPermission().then(() => {
        console.log('requested!');
      });
    }
  }

  parse_query_string(query) {
    var vars = query.split('&');
    var query_string = {};
    for (var i = 0; i < vars.length; i++) {
      var pair = vars[i].split('=');
      var key = decodeURIComponent(pair[0]);
      var value = decodeURIComponent(pair[1]);
      // If first entry with this name
      if (typeof query_string[key] === 'undefined') {
        query_string[key] = decodeURIComponent(value);
        // If second entry with this name
      } else if (typeof query_string[key] === 'string') {
        var arr = [query_string[key], decodeURIComponent(value)];
        query_string[key] = arr;
        // If third or later entry with this name
      } else {
        query_string[key].push(decodeURIComponent(value));
      }
    }
    return query_string;
  }

  handleDynamicLink = (link) => {
    // Handle dynamic link inside your own application
    console.log(link);

    if (
      link &&
      link.url &&
      link.url.includes('williamsburgtherapy.page.link/details')
    ) {
      let data = link.url.split('details?');
      if (data.length > 0) {
        data = link.url.split('details?')[1];

        var parsed_qs = this.parse_query_string(data);
        this.appId = parsed_qs.ci;
        console.log(this.appId);
        //alert(parsed_qs.ui);
        this.setState({
          appId: parsed_qs.ci,
          channelName: parsed_qs.cn,
          uid: parsed_qs.ui,
          linkJoin: true,
          // doctorName:parsed_qs.dn,
          joinSucceed: true,
          isStreamOpen: true,
        });

        this.init();
      }
      // setDeepLinkData(link)
    }
  };

  componentDidMount() {
    console.log(AppState.currentState);
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.backAction,
    );
    AppState.addEventListener('change', this.handleAppStateChange);

    dynamicLinks().onLink(this.handleDynamicLink);

    dynamicLinks()
      .getInitialLink()
      .then((link) => {
        if (
          link &&
          link.url &&
          link.url.includes('williamsburgtherapy.page.link/details')
        ) {
          let data = link.url.split('details?');
          if (data.length > 0) {
            data = link.url.split('details?')[1];

            var parsed_qs = this.parse_query_string(data);
            this.appId = parsed_qs.ci;
            console.log(this.appId);
            this.setState({
              appId: parsed_qs.ci,
              channelName: parsed_qs.cn,
              uid: parsed_qs.ui,
              linkJoin: true,
              joinSucceed: true,
              isStreamOpen: true,
            });
            this.init();
          }

          // setDeepLinkData(link)
        }
      });

    Linking.getInitialURL().then((url) => {
      if (url) {
        console.log(url);
      } else {
        console.log('no url');
        this.init();
      }
    });
  }

  backAction = () => {
    const {joinSucceed} = this.state;
    const showText = joinSucceed
      ? 'Are you sure you want to end the conversation now ?'
      : 'Are you sure you want to exit ?';
    Alert.alert('Hold on!', showText, [
      {
        text: 'Cancel',
        onPress: () => null,
        style: 'cancel',
      },
      {
        text: 'YES',
        onPress: () => (joinSucceed ? this.endCall() : BackHandler.exitApp()),
      },
    ]);
    return true;
  };
  handleAppStateChange = (nextAppState) => {
    if (nextAppState === 'background') {
      console.log('the app is closed');
      this.setState({
        isStreamOpen: false,
      });
    }
  };

  componentWillUnmount() {
    console.log(AppState.currentState);
    this.backHandler.remove();
    this.endCall();
    AppState.removeEventListener('change', this.handleAppStateChange);
  }
  /**
   * @name init
   * @description Function to initialize the Rtc Engine, attach event listeners and actions
   */
  init = async () => {
    console.log(AppState.currentState);
    const {appId} = this.state;
    const {isStreamOpen} = this.state;
    console.log(appId);
    console.log(this.appId);
    this._engine = await RtcEngine.create('3535a5b6b0414c58ad4edab659badb72');
    await this._engine.enableVideo();

    this._engine.addListener('Warning', (warn) => {
      console.log('Warning', warn);
    });

    this._engine.addListener('Error', (err) => {
      console.log('Error', err);
    });

    this._engine.addListener('UserJoined', (uid, elapsed) => {
      console.log('UserJoined', uid, elapsed);
      // Get current peer IDs
      const {peerIds} = this.state;
      // If new user
      if (peerIds.indexOf(uid) === -1) {
        this.setState({
          // Add peer ID to state array
          peerIds: [...peerIds, uid],
        });
      }
    });

    this._engine.addListener('UserOffline', (uid, reason) => {
      console.log('UserOffline', uid, reason);
      const {peerIds} = this.state;
      this.setState({
        // Remove peer ID from state array
        peerIds: peerIds.filter((id) => id !== uid),
      });
    });

    // If Local user joins RTC channel
    this._engine.addListener('JoinChannelSuccess', (channel, uid, elapsed) => {
      console.log('JoinChannelSuccess', channel, uid, elapsed);
      // Set state variable to true
      this.setState({
        joinSucceed: true,
      });
    });

    if (isStreamOpen) {
      this.startCall();
    }
    // this.startCall();
    //alert(isAppKill);
  };

  /**
   * @name startCall
   * @description Function to start the call
   */
  startCall = async () => {
    console.log('object');
    this.endCall();
    // Join Channel using null token and channel name
    //alert(this.state.uid)
    await this._engine?.joinChannel(
      null,
      this.state.channelName,
      this.state.uid,
      0,
    );
    // BackHandler.exitApp();
  };

  /**
   * @name endCall
   * @description Function to end the call
   */
  endCall = async () => {
    const {isStreamOpen} = this.state;
    await this._engine?.leaveChannel();
    this.setState({
      peerIds: [],
      joinSucceed: false,
      linkJoin: false,
      isStreamOpen: false,
    });
  };
  muteUnmute = async () => {
    const {audioMuted} = this.state;
    this._engine.muteLocalAudioStream(!audioMuted);
    this.setState({
      audioMuted: audioMuted ? false : true,
    });
  };
  changeCamera = async () => {
    //alert('Camera Switched');
    const {cameraSwitch} = this.state;
    this.setState({
      cameraSwitch: !cameraSwitch,
    });
    this._engine.switchCamera();
  };
  hideUnhide = async () => {
    const {videoHide} = this.state;
    this._engine.muteLocalVideoStream(!videoHide);
    this.setState({
      videoHide: videoHide ? false : true,
    });
  };

  render() {
    const {linkJoin} = this.state;

    return (
      <View style={styles.max}>
        {!linkJoin ? <View>{this._renderNoCallLayout()}</View> : null}
        {this._renderVideos()}
      </View>
    );
  }

  _renderVideos = () => {
    const {joinSucceed} = this.state;
    const {videoHide} = this.state;

    return joinSucceed ? (
      <View style={styles.containerView}>
        <Image
          style={styles.callLogoWtg}
          source={require('../assets/images/wtg.png')}
        />
        <View style={styles.fullView}>
          {!videoHide ? (
            <RtcLocalView.SurfaceView
              style={styles.localVideo}
              channelId={this.state.channelName}
              renderMode={VideoRenderMode.Hidden}
            />
          ) : (
            <FeatherIcon
              name="video-off"
              onPress={this.hideUnhide}
              style={styles.localVideoOff}></FeatherIcon>
          )}
          {this._renderRemoteVideos()}
          <View style={{marginLeft: '14%'}}>{this._renderButtons()}</View>
        </View>
      </View>
    ) : null;
  };

  _renderRemoteVideos = () => {
    const {peerIds} = this.state;
    return (
      <ScrollView
        style={styles.remoteContainer}
        // contentContainerStyle={{paddingHorizontal: 2.5}}
        horizontal={true}>
        {peerIds.map((value, index, array) => {
          return (
            <RtcRemoteView.SurfaceView
              style={styles.remote}
              uid={value}
              channelId={this.state.channelName}
              renderMode={VideoRenderMode.Hidden}
              zOrderMediaOverlay={true}
            />
          );
        })}
      </ScrollView>
    );
  };
  _renderButtons = () => {
    const {cameraSwitch} = this.state;
    const {audioMuted} = this.state;
    const {videoHide} = this.state;
    return (
      <View style={styles.containerButton}>
        <View style={styles.icon4Row}>
          <FeatherIcon
            name={!audioMuted ? 'mic' : 'mic-off'}
            style={[
              styles.icon,
              {fontSize: 25, paddingLeft: 18, paddingTop: 18},
            ]}
            onPress={this.muteUnmute}></FeatherIcon>

          <IoniconsIcon
            name={cameraSwitch ? 'camera-outline' : 'camera-reverse-outline'}
            style={styles.icon}
            onPress={this.changeCamera}></IoniconsIcon>

          <MaterialIcons
            name="call-end"
            onPress={this.endCall}
            style={styles.iconend}></MaterialIcons>

          <FeatherIcon
            name={!videoHide ? 'video' : 'video-off'}
            onPress={this.hideUnhide}
            style={[
              styles.icon,
              {fontSize: 25, paddingLeft: 18, paddingTop: 18},
            ]}></FeatherIcon>
        </View>
      </View>
    );
  };
  _renderNoCallLayout() {
    return (
      <>
        <View style={{flex: 1, flexDirection: 'column'}}>
          <View style={{height: 150, backgroundColor: '#fff'}}>
            <Image
              style={styles.lg}
              source={require('../assets/images/WTGLogo.png')}
            />
          </View>
          <View style={{backgroundColor: '#fff'}}>
            <Image
              style={styles.bg}
              source={require('../assets/images/bg.jpg')}
            />
          </View>
          <View style={{backgroundColor: '#fff', height: 40}}>
            <Text style={{textalign: 'center', alignSelf: 'center'}}>
              Please Join through the dashboard link
            </Text>
          </View>
          <View>
            <Text
              style={styles.joinLink}
              onPress={() => Linking.openURL('https://upsitez.store/')}>
              Join Now
            </Text>
          </View>
        </View>
      </>
    );
  }
}
