import {Dimensions, StyleSheet, Platform} from 'react-native';
import {Color} from 'react-native-agora';

const dimensions = {
  width: Dimensions.get('window').width,
  height: Dimensions.get('window').height,
};
const ratio = dimensions.width / 2000;
export default StyleSheet.create({
  max: {
    flex: 1,
    backgroundColor: '#EADAC9',
  },
  buttonHolder: {
    height: dimensions.width / 2,
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    //   position: 'absolute',
  },
  button: {
    paddingVertical: 8,
    backgroundColor: '#000',
    top: 90,
    width: '80%',
    textAlign: 'center',
    alignSelf: 'center',
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 25,
    textAlign: 'center',
  },
  fullView: {
    width: '92%',
    height: '85%',
    top: '5%',
    justifyContent: 'center',
    alignSelf: 'center',
  },
  localVideo: {
    flex: 1,
    alignItems: 'center',
  },
  containerView: {
    width: dimensions.width,
    height: dimensions.height,
    backgroundColor: '#EADAC9',
  },
  logo: {
    //fontFamily: 'Salernomi-J',
    fontSize: dimensions.width / 10,
    alignSelf: 'center',
    justifyContent: 'center',
    top: dimensions.height / 50,
  },
  remoteContainer: {
    width: '100%',

    position: 'absolute',
    bottom: dimensions.height / 9,
    flex: 2,
    flexDirection: 'row-reverse',
  },
  remote: {
    width: 110,
    height: 140,
    marginHorizontal: 2.5,
  },
  noUserText: {
    paddingHorizontal: 10,
    paddingVertical: 5,
    color: '#0093E9',
  },
  setBackground: {
    backgroundColor: 'red',
  },
  heading: {
    fontFamily: 'Salernomi-J',
    marginVertical: 20,
    fontSize: dimensions.width / 10,
    alignSelf: 'center',
    height: 100,
    lineHeight: 100,
    justifyContent: 'center',
    top: 150,
  },

  subheading: {
    fontFamily: 'Salernomi-J',
    fontSize: dimensions.width / 10,
    alignSelf: 'center',
    height: 100,
    lineHeight: 100,
    justifyContent: 'center',
    top: 70,

    //marginLeft: -20,
  },
  subheading2: {
    fontFamily: 'Salernomi-J',
    fontSize: dimensions.width / 20,
    alignSelf: 'center',
    height: 100,
    lineHeight: 100,
    justifyContent: 'center',
    top: dimensions.width / 85,
  },
  showName: {
    position: 'relative',
  },

  buttonControlls: {
    width: '100%',
    height: dimensions.height / 28,
    position: 'absolute',
    top: 5,
    flexDirection: 'row',
  },

  containerButton: {
    flex: 2,
    flexDirection: 'row',
    position: 'absolute',
    bottom: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },

  icon4Row: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '-3.5%',
  },

  iconend: {
    backgroundColor: 'red',
    fontSize: 30,
    color: '#fff',
    height: 60,
    width: 60,
    borderRadius: 40,
    margin: 5,
    paddingLeft: 15,
    paddingTop: 15,

    //position: 'absolute',
  },
  icon: {
    backgroundColor: '#fff',
    fontSize: 30,
    margin: 5,
    height: 60,
    width: 60,
    borderRadius: 50,
    paddingLeft: 15,
    paddingTop: 15,
    //position: 'absolute',
    //left: 25,
  },
  localVideoHide: {
    backgroundColor: 'white',
  },
  localVideoOff: {
    fontSize: 200,
    textAlign: 'center',
    height: '100%',
  },
  imgContainer: {
    flexDirection: 'row',
  },
  image: {
    resizeMode: 'cover',
    width: dimensions.width,
    height: dimensions.height / 30,
    top: 30,
    justifyContent: 'center',
    alignItems: 'center',
    //opacity: 0.4
  },
  bg: {
    resizeMode: 'cover',
    width: dimensions.width,
    height: dimensions.height,
  },
  lg: {
    resizeMode: 'contain',
    marginTop: Platform.OS === 'android' ? '5%' : '10%',
    marginLeft: '3%',
  },
  callLogoWtg: {
    resizeMode: 'contain',
    left: '40%',
    top:
      Platform.OS === 'android'
        ? '1.5%'
        : dimensions.height / dimensions.height + 3 + '%',
    // marginBottom: 20,
  },
  joinLink: {
    fontSize: 25,
    fontFamily: 'Salernomi-J',

    color: 'blue',
    //paddingLeft: 2,
    // paddingHorizontal: 6,
    // paddingVertical: 6,
    // paddingTop: 6,
    //marginLeft: 155,

    left: '38%',
    position: 'absolute',
  },
});
