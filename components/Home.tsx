import React from 'react'
import { Button, View, Text } from 'react-native';
import { useRoute } from '@react-navigation/native';



export default function Home({navigation}) {
  const route = useRoute();
  console.log(route)

   return (
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
          <Text>Home Screen</Text>
          <Button
            title="Go to Details"
            onPress={() => navigation.navigate('Call')}
          />
        </View>
      );

}
