import React, {useEffect, useState} from 'react';
import dynamicLinks from '@react-native-firebase/dynamic-links';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';

import SplashScreen from 'react-native-splash-screen';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';

import Home from './components/Home';
import Call from './components/Call';
const Stack = createStackNavigator();
const App = () => {
  const linking = {
    prefixes: ['williamsburgtherapy://'],

    config: {
      screens: {
        Call: 'williamsburgtherapy/:details?',
      },
    },
  };
  useEffect(() => {
    SplashScreen.hide();
    console.log('object');
    // const unsubscribe = dynamicLinks().onLink(handleDynamicLink);
    // // When the is component unmounted, remove the listener
    // return () => unsubscribe();
  }, []);

  return (
    <>
      <NavigationContainer linking={linking}>
        <Stack.Navigator headerMode={'none'} initialRouteName="Call">
          <Stack.Screen name="Call" component={Call} />
          {/* <Stack.Screen name="Home" component={Home} /> */}
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
};

export default App;
